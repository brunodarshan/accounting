FROM ruby:3.0.0

RUN mkdir /app
WORKDIR /app

COPY Gemfile* /app/
RUN bundle install
COPY . /app/

EXPOSE 3000
